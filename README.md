# sierra-autorenewal
* Script should be run as CRON or Automated task to eliminate timeouts
* Front-end display removed

#Configuration
* Edit config.php for your local setup


##API connection information
* set api_key (generate in Sierra Admin)
* set api_secret
* set server address

##Setup III database connection
* grant a user account permission to Sierra SQL Access in Sierra Admin
* add III database and credentials 

##Setup a local logging MySQL database
* create a MySQL database such as iiiapi
* add MySQL credentials 